//
//  ViewController.swift
//  ColorPalette
//
//  Created by Nakul Narwaria on 9/14/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var blueTextView: UITextField!
    @IBOutlet weak var greenTextView: UITextField!
    @IBOutlet weak var redTextView: UITextField!
    @IBOutlet weak var redColorSlider: UISlider!
    @IBOutlet weak var greenColorSlider: UISlider!
    @IBOutlet weak var blueColorSlider: UISlider!
    @IBOutlet weak var colorView: UIView!
    
    @IBAction func colorButtonPressed() {
        setView()
        dismissKeyboard()
    }
    
    @IBAction func redSliderValueChange(_ redSlider: UISlider) {
        redTextView.text = String(redSlider.value)
        setView()
    }
    
    @IBAction func greenSliderValueChange(_ greenSlider: UISlider) {
        greenTextView.text = String(greenSlider.value)
        setView()
    }
    
    @IBAction func blueSliderValueChange(_ blueSlider: UISlider) {
        blueTextView.text = String(blueSlider.value)
        setView()
    }
    
    func setView(){
        let red = unwrapFloatValues(redTextView.text);
        let blue = unwrapFloatValues(blueTextView.text);
        let green = unwrapFloatValues(greenTextView.text);
        setSliderValue(red, redColorSlider)
        setSliderValue(green, greenColorSlider)
        setSliderValue(blue, blueColorSlider)
        setColor(CGFloat(red), CGFloat(green), CGFloat(blue))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(saveData), name: UIApplication.didEnterBackgroundNotification, object: nil)
        let userDefaults = UserDefaults.standard
        blueTextView.text = userDefaults.string(forKey: "blue")
        redTextView.text = userDefaults.string(forKey: "red")
        greenTextView.text = userDefaults.string(forKey: "green")
        setView()
    }
    
    func unwrapFloatValues(_ color:String?)->Float{
        if color==nil{
            return 0;
        }
        guard let intValue = Float(color!) else { return 0; }
        return intValue;
    }

    @objc func saveData(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(redTextView.text, forKey: "red")
        userDefaults.set(greenTextView.text, forKey: "green")
        userDefaults.set(blueTextView.text, forKey: "blue")
    }
    
    func setColor(_ red:CGFloat=0,_ green:CGFloat=0,_ blue:CGFloat=0){
        let color = UIColor(red: red/100, green: green/100, blue: blue/100, alpha: 1)
        colorView.backgroundColor = color
    }
    
    func setSliderValue(_ data:Float, _ slider: UISlider){
        slider.value = data
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch! = touches.first
        colorView.center = touch.location(in: self.view)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch! = touches.first
        colorView.center = touch.location(in: self.view)
    }
}

