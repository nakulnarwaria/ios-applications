## Abstract 
---

This is an iOS application that contains three text fields (Red, Green Blue) with labels, one button (with label Color) and a UIView. The UIView we will call the color view. The color view starts with a black background. The user is to enter values 0 through 100 in the text fields. When one presses the button the background color of the color view is changed using the values that the user entered in the text fields.

The application also store the values in the text fields. So when the user "kills" the application and then restarts the app the values in the test fields are restored to the values they had before the app was killed and the color view is the same color is was before the app was killed.

The user ia also able to move the rectangle by touching the screen with one finger. When the user touches the screen the center of the rectangle is placed under the finger.

The app also includes sliders to allow the user to change the colors with three sliders. As a user changes the slider the value in the corresponding text field and the color of the color view also changes.

## Screenshot
---

Following is the view of the app described above -

![Screenshot](https://user-images.githubusercontent.com/12014727/54401704-f23a1c00-4685-11e9-9664-cecbe51f981c.png)