//
//  MapTabController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapTabController: UIViewController {

    var locationManager:CLLocationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        mapView!.region = triangulateSanDiego()
        mapView!.mapType = MKMapType.standard
        mapView!.showsUserLocation = true
        mapView!.showsTraffic = true
       
    }
    
    func triangulateSanDiego()-> MKCoordinateRegion {
        
        let center = CLLocationCoordinate2DMake(32.7157, -117.1611 )
        let widthMeters:CLLocationDistance = 100
        let heightMeters:CLLocationDistance = 1000*120
        return MKCoordinateRegion(center: center, latitudinalMeters: widthMeters, longitudinalMeters: heightMeters)
    }

}
