//
//  ViewController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/19/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var tab: UIButton!
    @IBOutlet weak var states: UIButton!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = NSLocalizedString("name", comment: "")
        passwordLabel.text = NSLocalizedString("password", comment: "")
        tab.setTitle(NSLocalizedString("tab", comment: ""), for: .normal)
        states.setTitle(NSLocalizedString("state", comment: ""), for: .normal)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TabBarController {
            destination.name = nameTextField.text!
        }
    }
    
    @IBAction func back(unwindSegue:UIStoryboardSegue) {
        if let source = unwindSegue.source as? StateSceneViewController {
        
        }
        if let source = unwindSegue.source as? NameTabController {
            nameTextField.text = source.nameTextField.text!
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "StateTransition" || identifier == "TabClick"  {
            let nameText:String = nameTextField.text ?? ""
            let passwordText:String = passwordTextField.text ?? ""
            if nameText.count < 4 || passwordText.count < 4 {
                return false
            }
        }
        return true
    }
}

