//
//  SportsTabController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class SportsTabController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate {
    
    var countryAndSports:Dictionary<String,Array<String>>?
    var countries:Array<String>?
    var selectedCountry:String?
    var sports:Array<String>?
    var selectedSport:String?
    
    @IBOutlet weak var sportsPicker: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard (countries != nil) && sports != nil else { return 0 }
        switch component {
        case 0: return countries!.count
        case 1: return sports!.count
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        guard (countries != nil) && sports != nil else { return "None" }
        
        switch component {
        case 0: return countries![row]
        case 1: return sports![row]
        default: return "None"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard (countries != nil) && sports != nil else { return }
        
        if component == 0 {
            selectedCountry = countries![row]
            sports = countryAndSports![selectedCountry!]!.sorted()
            pickerView.reloadComponent(1)
        }
        else{
            selectedSport = sports![row]
            showAlert()
        }
        
    }
    
    @IBAction func showAlert() {
        
        var messageString: String = "You have selected \(selectedSport!) of \(selectedCountry!)"
        let controller = UIAlertController(title: "You have chosen a sport!",
                                           message: messageString, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: {})
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let data:Bundle = Bundle.main
        let sportsPList:String? = data.path(forResource: "sportsList", ofType: "plist")
        if sportsPList != nil {
            countryAndSports = (NSDictionary.init(contentsOfFile: sportsPList!) as! Dictionary)
            countries = countryAndSports?.keys.sorted()
            selectedCountry = countries![0]
            sports = countryAndSports![selectedCountry!]!.sorted()
        }
        if sportsPicker != nil {
            sportsPicker!.delegate = self
            sportsPicker!.dataSource = self
        }
    }
    

}
