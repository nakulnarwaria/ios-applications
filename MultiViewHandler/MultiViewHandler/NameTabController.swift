//
//  NameTabController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class NameTabController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBar = tabBarController as! TabBarController
        nameTextField.text = String(describing: tabBar.name)
    }
    
}
