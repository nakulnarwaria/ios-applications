//
//  DetailSceneController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class DetailSceneController: UIViewController {

    var stateName: String = ""
    var abbreviation : String = ""
    var since : String = ""
    var capital : String = ""
    
    @IBOutlet weak var stateNameLabel: UILabel!
    @IBOutlet weak var abbreviationLabel: UILabel!
    @IBOutlet weak var capitalLabel: UILabel!
    @IBOutlet weak var sinceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stateNameLabel.text = stateName
        abbreviationLabel.text = abbreviation
        capitalLabel.text = capital
        sinceLabel.text = since
        
    }

   
}
