//
//  StateSceneViewController.swift
//  MultiViewHandler
//
//  Created by Nakul Narwaria on 10/19/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class StateSceneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var stateNameList:[String] = []
    var stateDictionary:[String : [String : String]] = [:]
    var selectedStateName:String!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateNameList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "tableCell")
        cell.textLabel?.text = stateNameList[indexPath.row]
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        selectedStateName = stateNameList[row]
        self.performSegue(withIdentifier: "detailLoad", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
       
    }
    
    func loadData(){
        if let path = Bundle.main.path(forResource: "stateList", ofType: "plist"),
            let myDict = NSDictionary(contentsOfFile: path){
            for (key,value) in myDict{
                if let keyString = key as? String{
                    stateNameList.append(keyString)
                    if let valueOfKey = value as? [String:String]{
                        stateDictionary[keyString] = valueOfKey
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailLoad" {
            if segue.destination.isKind(of: DetailSceneController.self) {
                let detailController = segue.destination as! DetailSceneController
                detailController.stateName = selectedStateName
                detailController.abbreviation = (stateDictionary[selectedStateName]!["abbreviation"]!)
                detailController.capital = (stateDictionary[selectedStateName]!["capital"]!)
                detailController.since = (stateDictionary[selectedStateName]!["since"]!)

            }
        }
    }
    
  
    @IBAction func back(unwindSegue:UIStoryboardSegue) {
        if let source = unwindSegue.source as? DetailSceneController {
            
        }
    }
    
}
