## App Description 
---

This app's main objective is to learn how to use different UI components and it will use a number of UI elements so the app will not make much sense as a single app. The app will be one with multiple scenes. Only portrait view and the iPhone 8 is supported for current version.

## Initial Scene
---

This scene contains two labels (Name, Password), two related textfields and two buttons. The buttons have titles States and Tab. The buttons only work with the name and password each contain text with more than three characters. The labels and buttons need to be localize to have default English but also support Spanish. The States button takes you to the States scene. The Tab button takes you to the Tab Scene. This is the only scene that needs to support Spanish.

## States Scene
---

The States scene shows in a Table view a list of 10 US states read from a plist. When the user taps on a state they go to a detailed scene which shows the name of the state, state abbreviation, the state capital and when it became a state. The user needs to be able to go back to the list of states. From the list of states they need to be able to go back to the Initial scene. The list of states can be found in plists.

## Tab Scene
---

The Tab scene has three tabs.

* **Name Tab** - 
This is the default tab that is displayed. It contains a text field, a label (Name) and a button, titled back. The textfield contains the name entered in the Initial scene. The user can edit the name in this view. The Back button takes the user back to the Initial scene. If the user edited the name in the Name tab the change is reflected Initial scene.

* **Sports Tab** - 
The view at this tab will contain a custom picker view with two columns. The first column will contain a list of countries. When the user has selected a country then the second column displays a list of sports in that country. The list of countries and their sports can be found in plists. When the user selects a sport an alert is shown indicating the country and sport selected.

* **Map Tab** -
This view contains a map view. The map is centered on San Diego and zoomed in so that El Cajon and Escondido are labeled in the map. Map view shows users current location. Map view also supports giving directions between two locations.  

## Screen shots - 
---

![Screenshot 1](https://user-images.githubusercontent.com/12014727/54403260-a2128800-468c-11e9-9963-435958d53528.png)
![Screenshot 2](https://user-images.githubusercontent.com/12014727/54403262-a2128800-468c-11e9-803b-914507374189.png)
![Screenshot 3](https://user-images.githubusercontent.com/12014727/54403263-a2128800-468c-11e9-889d-71718a321545.png)
![Screenshot 4](https://user-images.githubusercontent.com/12014727/54403264-a2128800-468c-11e9-9888-633f914c505d.png)
![Screenshot 5](https://user-images.githubusercontent.com/12014727/54403265-a2128800-468c-11e9-8bac-2e8053b39558.png)
![Screenshot 6](https://user-images.githubusercontent.com/12014727/54403266-a2ab1e80-468c-11e9-9241-6c72111c0c5f.png)
![Screenshot 7](https://user-images.githubusercontent.com/12014727/54403267-a2ab1e80-468c-11e9-9698-07508fe6624a.png)
![Screenshot 8](https://user-images.githubusercontent.com/12014727/54403268-a2ab1e80-468c-11e9-9926-3e2b35d7a23d.png)
![Screenshot 9](https://user-images.githubusercontent.com/12014727/54403270-a2ab1e80-468c-11e9-89cf-eaefa5b59418.png)



## Credits - 
---

State/Sport data provided by **Professor Roger Whitney** (San Deigo State University). 
