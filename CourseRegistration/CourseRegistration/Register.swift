//
//  SecondViewController.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/16/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class Register: UIViewController {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var redIdTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    var displayMessage:String = ""
    var displayAlertTitle = ""
    
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }

    @IBAction func registerClicked(_ sender: Any) {
        
        let RegisterStudentURL:String = NSLocalizedString("SIGN_UP_STUDENT_URL", comment: "")
       
        let parameters = [
            "firstname":self.firstNameTextField.text,
            "lastname":self.lastNameTextField.text,
            "redid": self.redIdTextField.text,
            "password": self.passwordTextField.text,
            "email": self.emailTextField.text
        ]
        
        Alamofire.request(RegisterStudentURL, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    let jsonObject = JSON as! NSDictionary
                    if let val = jsonObject["error"] {
                        self.displayAlertTitle = NSLocalizedString("ERROR_REGISTRATION", comment: "")
                        self.displayMessage = val as! String
                        self.showAlert()
                    
                    }
                    else{
                        self.displayAlertTitle = NSLocalizedString("SUCCESS", comment: "")
                        self.displayMessage = NSLocalizedString("REGISTRATION_SUCCESS", comment: "")
                        self.showAlert()
                        self.userDefaults.set(self.redIdTextField.text, forKey: "redid")
                        self.userDefaults.set(self.passwordTextField.text, forKey: "password")
                        self.userDefaults.synchronize()
                        self.returnToFirstTab()
                    }
                    
                }
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    @IBAction func showAlert() {
        
        let messageString: String = "\(displayMessage)"
        let controller = UIAlertController(title: displayAlertTitle,
                                           message: messageString, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler : nil)
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: {})
        
    }
    
    func returnToFirstTab(){
        
        
        self.tabBarController?.selectedIndex = 0
    }
}

