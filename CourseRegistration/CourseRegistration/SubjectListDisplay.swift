//
//  SubjectListDisplay.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class SubjectListDisplay: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var startTime:String = ""
    var endTime:String = ""
    var level:String = ""
    var majorId:String = ""
    var redId:String = ""
    var password:String = ""
    
    var alertMessage = ""
    var alertTitle = ""
    
    var subjectList:[Int] = []
    var subjectTitles:[String] = []
    var selectedSubject:Int = 0
    
    @IBOutlet weak var subjectTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectTitles.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "SubjectId")
        cell.textLabel?.text = "\(subjectTitles[indexPath.row])"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        if let first = subjectTitles[row].components(separatedBy: " ").first {
            self.selectedSubject = Int(first)!
        }
        self.performSegue(withIdentifier: "SearchDetail", sender: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        
        let parameters = [
            "subjectid":self.majorId,
            "level":self.level,
            "starttime":self.startTime,
            "endtime":self.endTime
        ]
        
        let url = NSLocalizedString("FILTERED_CLASS_ID_URL", comment: "")
        
        Alamofire.request(url, method:.get, parameters:parameters).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let subjectsIds = JSON as! NSArray
                    self.subjectList = []
                    if(subjectsIds.count == 0){
                        self.alertTitle = NSLocalizedString("NO_SUBJECTS", comment: "")
                        self.alertMessage = NSLocalizedString("SEARCH_UNSUCCESS", comment: "")
                        self.showAlert()
                        
                    }
                    for subjectId in subjectsIds{
                        self.subjectList.append(subjectId as! Int)
                    }
                    
                    var classDetailsUrl = NSLocalizedString("CLASS_DETAILS_URL", comment: "")
                    let parameters = [
                        "classids" : self.subjectList
                    ]
                    
                    // Nested network reqs for extracting titles of the subject ids received from first network req
                    Alamofire.request(classDetailsUrl, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            self.subjectTitles = []
                            if let JSON = response.result.value {
                                
                                let jsonObject = JSON as! NSArray
                                
                                self.subjectList = []
                                for subject in jsonObject{
                                    var course = subject as! NSDictionary
                                    var courseTitle:String = ""
                                    courseTitle = courseTitle + "\(course["id"] ?? "")" + " "
                                    courseTitle = courseTitle + "\(course["title"] ?? "")"
                                    self.subjectTitles.append(courseTitle)
                                }
                                
                                self.subjectTableView.reloadData()
                            }
                        case .failure(let error):
                            print(error)
                        }
                        
                    }
                }
            case .failure(let error):
                print(error)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailView {
            destination.redId = self.redId
            destination.password = self.password
            destination.search = true
            destination.subjectId = selectedSubject
        }
    }
    
    @IBAction func showAlert() {
        
        let messageString: String = "\(alertMessage)"
        let controller = UIAlertController(title: alertTitle,
                                           message: messageString, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default){
            action in NSLog("Navigation")
            self.navigationController?.popViewController(animated: true)
        }
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: {})
        
    }

}
