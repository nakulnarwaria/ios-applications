//
//  DetailView.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/17/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class DetailView: UIViewController {

    
    var subjectId:Int = 0
    var courseFull:Bool = false
    var search:Bool = false
    var redId:String = ""
    var password:String = ""
    var alertMessage = ""
    var alertTitle = ""
    var waitListAClass:Bool = false
    
    var parameters : [String : Any] = [:]
    
    
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var instructorLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var courseTitleLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var courseDescriptionTextView: UITextView!
    
    @IBOutlet weak var addOrRemove: UIButton!
    @IBAction func addOrRemovedClicked(_ sender: Any) {
        
        if (!search){
            var url = ""
            if (courseFull){
                url = NSLocalizedString("DROP_FROM_WAIT_LIST_URL", comment: "")
            }
            else{
                url = NSLocalizedString("DROP_REGISTERED_URL", comment: "")
            }
            
            removeClass(parameters, url)
        }
        else{
            var url = NSLocalizedString("ADD_ENROLL_URL", comment: "")
            addClass(parameters, url)
        }
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        if(search){
            addOrRemove.setTitle("Add",for: .normal)
        }
        else{
            addOrRemove.setTitle("Remove",for: .normal)
        }
        parameters = [
            "redid": self.redId,
            "password": self.password,
            "courseid": self.subjectId
        ]
    }
    
    func loadData(){
        let url = NSLocalizedString("CLASS_DETAILS_URL", comment: "")
        parameters = [
            "classid": self.subjectId
        ]
        
        Alamofire.request(url, method:.get, parameters:parameters).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let classInfo = JSON as! NSDictionary
                    if let val = classInfo["error"] {
                        self.alertTitle = "Error loading class details"
                        self.alertMessage = "\(val)"
                        self.showAlert()
                        return
                    }
                    self.startTimeLabel.text = "\(classInfo["startTime"] ?? " ")"
                    self.endTimeLabel.text = "\(classInfo["endTime"] ?? " ")"
                    self.daysLabel.text = "\(classInfo["days"] ?? " ")"
                    self.instructorLabel.text = "\(classInfo["instructor"] ?? " ")"
                    self.scheduleLabel.text = "\(classInfo["schedule#"] ?? " ")"
                    self.courseTitleLabel.text = "\(classInfo["fullTitle"] ?? " ")"
                    self.placeLabel.text = "\(classInfo["building"] ?? " ")"
                    self.courseDescriptionTextView.text = "\(classInfo["description"] ?? " ")"
                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    
    func removeClass(_ parameters : [String : Any], _ url:String){
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON = response.result.value {
        
                            let jsonObject = JSON as! NSDictionary
        
                            if let val = jsonObject["error"] {
                                self.alertMessage = "\(val)"
                                self.alertTitle = NSLocalizedString("CLASS_REMOVAL_REQ", comment: "")
                                self.showAlert()
                                return
                            }
                            self.alertMessage = NSLocalizedString("CLASS_REMOVAL_SUCCESS", comment: "")
                            self.alertTitle = NSLocalizedString("CLASS_REMOVAL_REQ", comment: "")
                            self.showAlert()
                            self.search = true
                            self.addOrRemove.setTitle("Add", for: .normal)
                            
                            // For updation of registered classes
                            NotificationCenter.default.post(name: NSNotification.Name("loadClassDetailsForStudent"), object: self)
                        }
                    case .failure(let error):
                        print(error)
                    }
        }
    }
    
    func addClass(_ parameters : [String : Any], _ url:String){
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        if("\(val)" == "Course is full"){
                            self.waitListAClass = true
                            self.alertTitle = NSLocalizedString("CLASS_FULL", comment: "")
                            self.alertMessage = NSLocalizedString("ADD_WAITLIST_QUESTION", comment: "")
                            self.showAlert()
                            self.waitListAClass = false
                        }
                        else{
                            self.alertMessage = "\(val)"
                            self.alertTitle = NSLocalizedString("CLASS_ADD_REQ", comment: "")
                            self.showAlert()
                        }
                        return
                    }
                    self.alertMessage = NSLocalizedString("CLASS_ENROLLED", comment: "")
                    self.alertTitle = NSLocalizedString("CLASS_ADD_REQ", comment: "")
                    self.showAlert()
                    self.search = false
                    self.addOrRemove.setTitle("Remove", for: .normal)
                    // For updation of registered classes
                    NotificationCenter.default.post(name: NSNotification.Name("loadClassDetailsForStudent"), object: self)
                    
                }
            case .failure(let error):
                print(error)
                self.popCurrentView()
            }
        }
    }
    
    
    @IBAction func showAlert() {
        
        let messageString: String = "\(alertMessage)"
        let controller = UIAlertController(title: alertTitle,
                                           message: messageString, preferredStyle: .alert)
        
        if(waitListAClass){
            let okAction = UIAlertAction(title: "Ok", style: .default){
                action in NSLog("Navigation")
                var waitListUrl = NSLocalizedString("ADD_TO_WAIT_LIST_URL", comment: "")
                self.addClass(self.parameters,waitListUrl)
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            controller.addAction(okAction)
            controller.addAction(cancelAction)
            self.present(controller, animated: true, completion: {})
        }
        else{
            let okAction = UIAlertAction(title: "Ok", style: .default){
                action in NSLog("Navigation")
                self.popCurrentView()
            }
            controller.addAction(okAction)
            self.present(controller, animated: true, completion: {})
            
        }
        
    }
    
    func popCurrentView(){
        navigationController?.popViewController(animated: true)
    }
   
}
