//
//  FilterParameters.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/21/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class FilterParameters: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate {
    
    @IBOutlet weak var startTimeTextField: UITextField!
    @IBOutlet weak var endTimeTextField: UITextField!
    @IBOutlet weak var levelPickerView: UIPickerView!
   
    var majorId:String = ""
    var selectedLevel: String?
    let timePattern: String = "([01]?[0-9]|2[0-3])[0-5][0-9]"
    var redId:String = ""
    var password:String = ""
    
    var alertMessage = ""
    var alertTitle = ""
    
    
    var levels:[String]?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard (levels != nil) else { return 0 }
        
        return levels!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int,
                    forComponent component: Int) -> String? {
        
        guard (levels != nil) else { return "None" }
        
        return levels![row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int,
                    inComponent component: Int) {
        
        guard (levels != nil) else { return }
        if component == 0 {
            selectedLevel = levels![row]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPickerViewData()
        
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    func loadPickerViewData(){
        
        let data:Bundle = Bundle.main
        let subjectLevelList:String? = data.path(forResource: "SubjectLevels", ofType: "plist")
        
        if subjectLevelList != nil {
            levels = (NSArray.init(contentsOfFile: subjectLevelList!) as! Array)
        }
        
        if levelPickerView != nil {
            levelPickerView!.delegate = self
            levelPickerView!.dataSource = self
        }
    }
    
    
    @IBAction func searchClicked(_ sender: Any) {
        
        if !isTimeValid(startTimeTextField!.text ?? ""){
            self.alertTitle = NSLocalizedString("INVALID_VALUES", comment: "")
            self.alertMessage = NSLocalizedString("INVALID_TIME_FORMAT", comment: "")
            self.showAlert()
        }
        else if !isTimeValid(endTimeTextField!.text ?? ""){
            self.alertTitle = NSLocalizedString("INVALID_VALUES", comment: "")
            self.alertMessage = NSLocalizedString("INVALID_TIME_FORMAT", comment: "")
            self.showAlert()
        }
        else {
            performSegue(withIdentifier: "displaySubjects", sender: sender)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SubjectListDisplay {
            destination.startTime = self.startTimeTextField.text ?? ""
            destination.endTime = self.endTimeTextField.text ?? ""
            destination.level = self.selectedLevel ?? ""
            destination.majorId = self.majorId
            destination.redId = self.redId
            destination.password = self.password
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false;
    }
    
    func isTimeValid(_ text:String) -> Bool{
        if text == ""{
            return true
        }
        return text.matches(self.timePattern)
    }
    
    @IBAction func showAlert() {
        
        let messageString: String = "\(alertMessage)"
        let controller = UIAlertController(title: alertTitle,
                                           message: messageString, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler : nil)
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: {})
        
    }
}

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
