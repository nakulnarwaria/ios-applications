//
//  DashboardTabBarController.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/20/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit

class DashboardTabBarController: UITabBarController{

    var redid:String = ""
    var password:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Load waitlisted and registered classes
        NotificationCenter.default.post(name: NSNotification.Name("loadClassDetailsForStudent"), object: self)
        
    }
    
}
