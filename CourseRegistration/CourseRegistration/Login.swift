//
//  FirstViewController.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/16/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class Login: UIViewController {

    var errorMessage:String = ""
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userNameTextField.text = userDefaults.string(forKey: "redid")
        self.passwordTextField.text = userDefaults.string(forKey: "password")
    }
    
    
    @IBAction func LoginClicked(_ sender: Any) {
        
        guard let name = self.userNameTextField.text else {
            self.errorMessage = NSLocalizedString("USER_NAME_EMPTY", comment: "")
            self.showAlert()
            return
        }

        guard let password = self.passwordTextField.text else{
            self.errorMessage = NSLocalizedString("PASSWORD_EMPTY", comment: "")
            self.showAlert()
            return
        }
        
        let parameters = [
            "redid": self.userNameTextField.text,
            "password": self.passwordTextField.text
        ]
        let url = NSLocalizedString("STUDENT_ENROLLED_CLASSES", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        
                        self.errorMessage = val as! String
                        self.showAlert()
                        return
                    }
                    
                    self.userDefaults.set(self.userNameTextField.text, forKey: "redid")
                    self.userDefaults.set(self.passwordTextField.text, forKey: "password")
                    self.userDefaults.synchronize()
                    
                    self.performSegue(withIdentifier: "signin", sender: sender)
                    
                    
                }
            case .failure(let error):
                print(error)
            }
        
        }
        
    }

    @IBAction func showAlert() {
        
        let messageString: String = "\(errorMessage)"
        let controller = UIAlertController(title: NSLocalizedString("ERROR_LOG_IN", comment: ""),
                                           message: messageString, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: {})
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DashboardTabBarController {
            destination.redid = self.userNameTextField.text ?? ""
            destination.password = self.passwordTextField.text ?? ""
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false;
    }
    
    @IBAction func unwindToRootViewController(segue: UIStoryboardSegue) {
        
    }
    
}

