//
//  CourseList.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/17/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class CourseList: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var majorTableViewDisplay: UITableView!
    
    var majorsList:[String] = []
    var majorIdList:[String] = []
    var selectedMajor = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return majorsList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "tableCell")
        cell.textLabel?.text = majorsList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        selectedMajor = majorIdList[row]
        self.performSegue(withIdentifier: "filter", sender: self)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
    }
    
    func loadData(){
        
        let searchMajorURL:String = NSLocalizedString("MAJOR_LIST_URL", comment: "")
        
        Alamofire.request(searchMajorURL, method:.get,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    var majorDetails:[NSDictionary] = (JSON as! NSArray) as! [NSDictionary]
                    for major in majorDetails{
                        self.majorsList.append("\(major["id"]!) \(major["title"]!)")
                        self.majorIdList.append("\(major["id"]!)")
                    }
                    self.majorTableViewDisplay.reloadData()
            }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filter" {
                let filterController = segue.destination as! FilterParameters
                let dashboard = tabBarController as! DashboardTabBarController
                filterController.redId = dashboard.redid
                filterController.password = dashboard.password
                filterController.majorId = self.selectedMajor
        }
    }
    


}
