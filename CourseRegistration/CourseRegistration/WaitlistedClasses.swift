//
//  WaitlistedClasses.swift
//  CourseRegistration
//
//  Created by Nakul Narwaria on 11/17/18.
//  Copyright © 2018 Nakul Narwaria. All rights reserved.
//

import UIKit
import Alamofire

class WaitlistedClasses: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var waitListTableView: UITableView!
    
    var subjectList:[Int] = []
    var subjectTitles:[String] = []
    var selectedSubject:Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectTitles.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "registeredCell")
        cell.textLabel?.text = "\(subjectTitles[indexPath.row])"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        if let first = subjectTitles[row].components(separatedBy: " ").first {
            self.selectedSubject = Int(first)!
        }
        self.performSegue(withIdentifier: "waitListDetail", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For initial load of data
        NotificationCenter.default.addObserver(self, selector:  #selector(loadWaitListData), name: NSNotification.Name("loadClassDetailsForStudent"), object: nil)
        loadWaitListData()
        
    }
    
    
    @objc func loadWaitListData(){
        
        let dashboard = tabBarController as! DashboardTabBarController
        let parameters = [
            "redid": dashboard.redid,
            "password": dashboard.password
        ]
        let url = NSLocalizedString("STUDENT_ENROLLED_CLASSES", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        return
                    }
                    
                    var registeredClassInfo = jsonObject as! [String : [Int]]
                    self.subjectList = registeredClassInfo["waitlist"] ?? []
                    
                    var classDetailsUrl = NSLocalizedString("CLASS_DETAILS_URL", comment: "")
                    let parameters = [
                        "classids" : self.subjectList
                    ]
                    
                    // Nested network reqs for extracting titles of the subject ids received from first network req
                    Alamofire.request(classDetailsUrl, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            self.subjectTitles = []
                            if let JSON = response.result.value {
                                
                                let jsonObject = JSON as! NSArray
                                
                                self.subjectList = []
                                for subject in jsonObject{
                                    var course = subject as! NSDictionary
                                    var courseTitle:String = ""
                                    courseTitle = courseTitle + "\(course["id"] ?? "")" + " "
                                    courseTitle = courseTitle + "\(course["title"] ?? "")"
                                    self.subjectTitles.append(courseTitle)
                                }
                                
                                
                                self.waitListTableView.reloadData()
                            }
                        case .failure(let error):
                            print(error)
                        }
                        
                    }
                    
                }
            case .failure(let error):
                print(error)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailView {
            let dashboard = tabBarController as! DashboardTabBarController
            destination.redId = dashboard.redid
            destination.password = dashboard.password
            destination.courseFull = true
            destination.search = false
            destination.subjectId = selectedSubject
            
        }
    }
    
}
