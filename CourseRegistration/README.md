## App description 
---

This app is a light and application version of a course registration website, which allows user to log in, watch their registered/wait-listed courses, and add/remove courses to the list. The main objective of this application is to learn how to make **networking apps**.

The app will allow users to register for classes. A student needs to enter their personal information - first name, last name, SDSU red id, email address and a password. The user can register for up to three courses and add themselves to waitlists for courses that are full. The student can also drop classes that they are registered for or drop themselves from waitlists.

The app will allow students to select from a list of courses filtered via major (or subject like Computer Science, Physics, etc), level of the course (lower division - 100 & 200 level courses, upper division courses 300, 400, 500 level courses, and graduate courses (500 level and higher), and time of day (classes starting after a given time and/or ending before a given time).  The app also display the courses the student is enrolled in and the courses that they are
waitlisted for. 

The app also stores the students personal data on the device so that they do not have to enter the data each time they use the app.

## Server Interaction Overview
---

A brief overview of the commands that are used to send to the server and what they do - 

* **subjectlist** - Returns a list of majors or subjects. Includes the name of the subject, the college it is in allowing the classes to be grouped by college.
* **classidslist** - Returns a list of courses based on subject(s), level, and time. Returns just the ids of the courses.
* **classdetails** - Given a course id returns information about the course: title, instructor, meeting time and place, etc.
* **addstudent** - Given the personal information about the student added the student to the server so they can add classes.
* **registerclass** - Given a course id, student�s red id and password registers the student in the class.
* **waitlistclass** - If a class is full given a course id, student�s red id and password adds the student to the wait list of a class. If a student drops the course the server does not enroll a student from the waitlist in the course.
* **unregisterclass** - Drops a student from the course.
* **unwaitlistclass** - Removes the student from a course waitlist.
* **resetstudent** - Drops the student from all courses and removes them from all waitlists.

## Screenshots of app - 
---

![Screenshot 1](https://user-images.githubusercontent.com/12014727/54402299-c3717500-4688-11e9-963c-db56af5b7f32.png)
![Screenshot 2](https://user-images.githubusercontent.com/12014727/54402307-c8cebf80-4688-11e9-93e0-36f00e77ad24.png)
![Screenshot 3](https://user-images.githubusercontent.com/12014727/54402412-1fd49480-4689-11e9-94e7-0ea31a4623ce.png)
![Screenshot 4](https://user-images.githubusercontent.com/12014727/54402428-295dfc80-4689-11e9-9865-09b0b0218ddf.png)
![Screenshot 5](https://user-images.githubusercontent.com/12014727/54402311-d2582780-4688-11e9-9ad0-a5e607600db2.png)
![Screenshot 6](https://user-images.githubusercontent.com/12014727/54402444-3bd83600-4689-11e9-9be2-f5762d60f8ac.png)
![Screenshot 7](https://user-images.githubusercontent.com/12014727/54402456-485c8e80-4689-11e9-8037-e853143146b0.png)
![Screenshot 8](https://user-images.githubusercontent.com/12014727/54402490-6b873e00-4689-11e9-881d-e919609f765f.png)
![Screenshot 9](https://user-images.githubusercontent.com/12014727/54402491-6b873e00-4689-11e9-9482-abb30ec30ec7.png)

## Credits
---

Server details provided by **Professor Roger Whitney** (San Diego State University).