## App Description 
---

The application will not actual do anything. **The entire app is just to learn how to use constraints, stack views and size classes to deal with different screen size, landscape and portrait view**. Below is the view of the application on an iPhone 8. Note how the Name and Password labels line up and the corresponding fields line up and are the same length. Also note that labels have a gray background so we can see where they start and end.

![Target View](https://user-images.githubusercontent.com/12014727/54404265-2a465c80-4690-11e9-94b5-1b8b77049b7c.png)

## Constraints
---

* In landscape there is more horizontal space. The issue is where should the extra space go? There is also less vertical space so one has to make sure that everything is still on the screen.
* In landscape the four buttons should remain in the corners and remain the same size. That is the spacing between a button and the nearest edges should not change.
* The name and password sections should remain pinned to the left side and remain the same size. That is all the extra space should appear after the textfields. Horizontally this section should remain centered.
* **Top row** - Button1, label1, label2, Button2. The extra horizontal space should be placed equally between Button1-label1, Label1-Label2, Label2-Button2. None should change size.
* **Second row** - Label5, Label6. The extra horizontal space should be used to grow the size of Label5 and Label6. That is the space between the Label5 & Label6 should not change and the spacing from the edge to the labels should not change. Horizontally this row stays between the first row and the name-password section.
* **Bottom row** - Button4, label4, label3, Button3. All the extra horizontal space goes between Label 4 and Label 3. The buttons and labels do not change size and the spacing between Button 4 and Label 4 does not change. The spacing between Label 3 and Buttons 3 does not change.


## Other Devices
---

iPad Pro 9.7 - Portrait View. Same as iPhone 8 with two changes. First the top row and the bottom row change places. That is Button1, label1, label2, Button2 will appear on the bottom and Button4, label4, label3, Button3 will appear on the top. The second change is that the background color for Label 5 and Label 6 should be red. iPhone 8 Plus, iPhone SE. They should appear like the iPhone 8. Support the iPhone XS Max. This should behave like the iPhone 8. The top row should remain
horizontally aligned and about 10 pixels down from the notch.

## Screenshots/Outputs - 
---

![Screenshot 1](https://user-images.githubusercontent.com/12014727/54404713-b1e09b00-4691-11e9-890c-ef91ce64c712.png)

![Screenshot 2](https://user-images.githubusercontent.com/12014727/54404714-b1e09b00-4691-11e9-89f5-8bfb842021de.png)

![Screenshot 3](https://user-images.githubusercontent.com/12014727/54404715-b1e09b00-4691-11e9-8d4a-80795bb03cec.png)

![Screenshot 4](https://user-images.githubusercontent.com/12014727/54404716-b2793180-4691-11e9-8b7d-a28aa8820e7b.png)

![Screenshot 5](https://user-images.githubusercontent.com/12014727/54404717-b2793180-4691-11e9-80cc-0733a88ab4c3.png)

![Screenshot 6](https://user-images.githubusercontent.com/12014727/54404718-b2793180-4691-11e9-8840-92381fdefb65.png)

