## Abstract
---

This repository contains practice iOS applications to understand and learn the iOS app development concepts. Visit individual project directories for detailed documentation.