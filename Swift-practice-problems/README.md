## Abstract
---

This playground contains different samples of swift dealing with optionals, closures and lambda functions. Below are the descriptions of problem solved in this playground

## Problems
---

1. Create a function squaredSums with one argument an Array of Int. Using the forEach
return the sum of the squares of the elements of the array. So an input of [1,2,3] would
return 1 + 2*2 + 3*3 = 14.  

2. Create a function squaredSums2 with one argument an Array of Int. Using the forEach
return the sum of the squares of the even elements of the array. So an input of [1,2,3]
would return 2*2 = 4.    
  
3. Create a function squaredSums3 with one argument an Array of optional Ints. Using
the forEach return the sum of the squares of the even elements of the array. So an input
of [1,2,3,nil] would return 2*2 = 4.  
  
4. Create a function squaredSums4 with one argument an optional Array of Ints. Using
the forEach return an optional Int, the sum of the squares of the even elements of the
array. So an input of [1,2,3] would return 2*2 = 4.  
  
5. Create a function squaredSums5 with one argument an Array of Int. Using the filter
and reduce return the sum of the squares of the even elements of the array. So an input of
[1,2,3] would return 2*2 = 4.  
  
6. Create a function squaredSums6 with one argument a Collection of Int. Using the
filter and reduce return the sum of the squares of the even elements of the array. So an input
of [1,2,3] would return 2*2 = 4.     
  
7. Create a Student Struct that contains a name, red id, number of units taken and
GPA. The struct needs one method priority which returns the number of units taken times
the GPA.  
  
8. Create priority queue class. The add method added elements to the queue. The
first method returns the element with the highest priority. The removeFirst removes the
element with the highest priority and returns it. The queue needs to be able to hold Student
structs from problem 7.  