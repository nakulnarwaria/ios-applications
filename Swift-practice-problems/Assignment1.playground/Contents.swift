//Question 1
func squaredSums(_ numbers: [Int])->Int{
    var total:Int = 0
    numbers.forEach{ value in
        total = total+value*value
    }
    return total
}
squaredSums([5,1,9,2]) //Functionality Check

//Question 2
func squaredSums2(_ numbers: [Int])->Int{
    var total:Int = 0
    numbers.forEach{ value in
        if value%2==0{
            total = total+value*value
        }
    }
    return total
}
squaredSums2([7,33,8,2]) //Functionality Check

//Question 3
func squaredSums3(_ numbers: [Int?])->Int{
    var total:Int = 0
    numbers.forEach{ num in
        if let value  = num{
            if (value%2==0){
                total = total + value*value
            }
        }
    }
    return total
}
//Functionality Check
var optionalInput: [Int?] = [1,2,3,nil]
squaredSums3(optionalInput)
squaredSums3([5,2,8,3])

//Question 4
func squaredSums4(_ numbers: [Int]?)->Int?{
    if let intInput = numbers {
        var total = 0
        intInput.forEach { value in
            if value%2 == 0 {
                total += value*value
            }
        }
        return total
    }
    return nil
}
//Functionality Check
squaredSums4(nil);
squaredSums4([2,7,4,0])

//Question 5
func squaredSums5(_ numbers: [Int])->Int{
    let total: Int
    func isEven(value: Int) -> Bool {
        return value%2==0
    }
    total = numbers.filter(isEven).reduce(0, { result,value in
        result + value*value
    })
    return total;
}
//Functionality Check
squaredSums5([0,4,12,7])

//Question 6
func squaredSums6<T: Collection>(_ numbers: T) -> Int where T.Element == Int {
    var total: Int = 0;
    numbers.forEach { value in
        if value%2 == 0 {
            total += value*value
        }
    }
    return total;
}

func squaredSums6<T:Collection>(_ numbers: T) -> Int where T.Element == (key: Int,value: Int) {
    var total: Int = 0;
    numbers.forEach { key,value in
        if value%2 == 0 {
            total += value*value
        }
    }
    return total;
}
//Functionality Check
squaredSums6([5,2,6,1]);
squaredSums6([1:1,2:2])
let setInput: Set = [5,2,9,1]
squaredSums6(setInput)

//Question 7
struct Student{
    let redID: Int;
    let name: String;
    
    var numberOfUnits: Int;
    var gpa: Double;
    
    func priority() -> Double {
        return Double(numberOfUnits)*gpa
    }
}
//Functionality Check
var student: Student = Student(redID: 821476104, name:"Nakul", numberOfUnits: 150, gpa: 3.95)
print("Printing student priority: \(student.priority())\n")

class PriorityQueue {
    
    private var studentList:[Student]
    
    public init(){
        studentList =  Array()
    }
    
    /*
     * Helper Methods for Max Heap implementation
     */
    private func getLeftChildIndex(_ parentIndex:Int)->Int
    {
        return 2*parentIndex + 1
    }
    private func getRightChildIndex(_ parentIndex:Int)->Int{
        return 2*parentIndex + 2
    }
    private func getParentIndex(_ childIndex:Int)->Int{
        return (childIndex-1)/2
    }
    
    private func hasLeftChild(_ index:Int)->Bool{
        return getLeftChildIndex(index)<self.getSize()
    }
    private func hasRightChild(_ index:Int)->Bool{
        return getRightChildIndex(index)<self.getSize()
    }
    private func hasParent(_ index:Int)->Bool{
        return getParentIndex(index)>=0
    }
    
    private func leftChild(_ index:Int)->Student{
        return studentList[getLeftChildIndex(index)];
    }
    private func rightChild(_ index:Int)->Student{
        return studentList[getRightChildIndex(index)];
    }
    private func parent(_ index:Int)->Student{
        return studentList[getParentIndex(index)];
    }
    
    private func swap(_ indexOne:Int, _ indexTwo:Int){
        let swapVariable:Student  = studentList[indexOne];
        studentList[indexOne] = studentList[indexTwo];
        studentList[indexTwo] = swapVariable;
    }
    
    /*
     * Returns the student object with highest priority without removing it from the heap
     */
    public func first()->Student{
        return studentList[0];
    }
    
    /*
     * Returns the student object with highest priority and removes it from the heap.
     */
    public func removeFirst()->Student{
        swap(0,studentList.count-1);
        let studentWithMaxPriority: Student = studentList.remove(at: studentList.count-1);
        restoreHeapStateDown();
        return studentWithMaxPriority;
    }
    
    /*
     * Adds a student to the heap, swap it with the top priority element in the heap,
     * and make the heap go back in stable state.
     */
    public func add(_ newStudent:Student){
        studentList.append(newStudent)
        restoreHeapStateUp();
    }
    
    /*
     * Takes the last entered element in the heap, and swap it with it's parent if priority
     * is higher, until heap is stable.
     */
    public func restoreHeapStateUp(){
        var index:Int = studentList.count-1;
        while(hasParent(index) && parent(index).priority() < studentList[index].priority()){
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }
    
    /*
     * Takes the top element and swap it with appropriate child if it's priority is lower
     * than it's children, until heap is stable.
     */
    public func restoreHeapStateDown(){
        var index:Int=0;
        while(hasLeftChild(index)){
            var largerPriorityIndex:Int = getLeftChildIndex(index);
            if( hasRightChild(index) && rightChild(index).priority() > leftChild(index).priority()){
                largerPriorityIndex = getRightChildIndex(index);
            }
            
            if(studentList[index].priority() > studentList[largerPriorityIndex].priority()){
                break;
            }
            else{
                swap(index, largerPriorityIndex);
            }
            index = largerPriorityIndex;
        }
    }
    
    /*
     * Returns list of all the students in the heap, sorted by priority.
     */
    public func displayStudent()->[Student]{
        var prioritywiseList:[Student] = Array()
        let currentHeapCopy:[Student] = Array(studentList)
        
        for i in 0...studentList.count-1 {
            var nextPriority:Student = self.removeFirst();
            print("Name: \(nextPriority.name) RedId: \(nextPriority.redID) Priority: \(nextPriority.priority()) \n");
            prioritywiseList.append(nextPriority);
        }
        studentList=currentHeapCopy
        return prioritywiseList;
    }
    
    public func getSize()->Int {
        return studentList.count;
    }
}

//TESTING PRIORITY QUEUE
var student1: Student = Student(redID: 8888888, name: "Student1", numberOfUnits: 50, gpa: 3.2)

var student2: Student = Student(redID: 8180481, name: "Stundent2", numberOfUnits: 120, gpa: 3.3)

var student3: Student = Student(redID: 8655352, name: "Student3", numberOfUnits: 130, gpa: 3.7)

var student4: Student = Student(redID: 8224234, name: "Student4", numberOfUnits: 60, gpa: 4)

var student5: Student = Student(redID: 8341412, name: "Student5", numberOfUnits: 100, gpa: 3.75)

var queue: PriorityQueue = PriorityQueue()

queue.add(student1)
queue.add(student2)
queue.add(student3)
queue.add(student4)
queue.add(student5)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.displayStudent()

print("-------------------Printing Highest Priority student and removing it from queue-----------------------\n")
print(queue.removeFirst(),"\n")

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.displayStudent()

print("\n First :\(queue.first()), Size of Queue: \(queue.getSize())\n")


